package com.example.myapplicationcompose

enum class Control(val code: Int) {
    OP2_STRENGTH(111),
    OP3_STRENGTH(121),

    OP2_FREQ(112),
    OP3_FREQ(122),

    DELAY(0),
    REVERB_MIX(1),
    REVERB_DECAY(2),
    FILTER_CUTOFF(3),

    OP1_ATTACK(103),
    OP1_DECAY(104),
    OP1_LEVEL(105),
    OP1_RELEASE(106),

    OP2_ATTACK(113),
    OP2_DECAY(114),
    OP2_LEVEL(115),
    OP2_RELEASE(116),
}