package com.example.myapplicationcompose

import android.content.Context
import android.content.pm.PackageManager
import android.media.midi.MidiDevice
import android.media.midi.MidiDeviceInfo
import android.media.midi.MidiManager
import android.media.midi.MidiReceiver
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.MotionEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import com.example.myapplicationcompose.ui.theme.MyApplicationComposeTheme
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.parcelize.Parcelize
import java.util.*
import kotlin.math.pow


interface SynthControl {
    fun note_on(frequency: Float, velocity: Float) {}
    fun note_off(frequency: Float) {}
    fun handle_change(control_num: Int, value: Float) {}
    fun arp_switch(frequency: Float) {}
}

val NOTE_ON = -112;
val NOTE_OFF = -128;

class SynthControlMock : SynthControl {}

class CustomMidiReceiver(synth_control: SynthControl, midi_updates: Channel<Pair<Int, Float>>) :
    MidiReceiver() {
    val _notes = create_note_to_freq()
    val midi_notes = _notes.second
    val synth_control = synth_control
    val midi_updates = midi_updates

    override fun onSend(data: ByteArray?, offset: Int, count: Int, timestamp: Long) {
        data?.let {
//            println(data.toString())
//            println(count)
            var message = data // data.slice(offset..offset + 3)

//            for (byte in message) {
//                print((byte.toInt()).toString() + " ")
//            }
//            println(" - message")

            if (count != 3) {
                return
            }

            // If CC msg
            if (message[0].compareTo(-80) == 0) {

                // 112 74 71 76 77 93 73 75
                // 114 18 19 16 17 91 69 72
                val controller = message[1]!!.toInt()
                val value: Float = message[2]!!.toInt() / 127.0f

                when (controller) {
                    112 -> {
                        midi_updates.trySend(Pair(Control.OP2_FREQ.code, value))
                    }

                    114 -> {
                        midi_updates.trySend(Pair(Control.OP3_FREQ.code, value))
                    }

                    74 -> {
                        midi_updates.trySend(Pair(Control.OP2_STRENGTH.code, value))
                    }
                    1 -> {
                        midi_updates.trySend(Pair(Control.OP2_STRENGTH.code, value))
                    }
                    18 -> {
                        midi_updates.trySend(Pair(Control.OP3_STRENGTH.code, value))
                    }
                    71 -> {
                        midi_updates.trySend(Pair(0, value))
                    }
                    19 -> {
                        midi_updates.trySend(Pair(1, value))
                    }

                    76 -> {
                        midi_updates.trySend(Pair(Control.OP2_FREQ.code, value))
                    }

                    16 -> {
                        midi_updates.trySend(Pair(Control.OP3_FREQ.code, value))
                    }

                    // Envelope OP1
                    77 -> {
                        midi_updates.trySend(Pair(Control.OP1_ATTACK.code, value))
                    }
                    93 -> {
                        midi_updates.trySend(Pair(Control.OP1_DECAY.code, value))
                    }
                    73 -> {
                        midi_updates.trySend(Pair(Control.OP1_LEVEL.code, value))
                    }
                    75 -> {
                        midi_updates.trySend(Pair(Control.OP1_RELEASE.code, value))
                    }

                    // Envelope OP2
                    17 -> {
                        midi_updates.trySend(Pair(Control.OP2_ATTACK.code, value))
                    }
                    91 -> {
                        midi_updates.trySend(Pair(Control.OP2_DECAY.code, value))
                    }
                    79 -> {
                        midi_updates.trySend(Pair(Control.OP2_LEVEL.code, value))
                    }
                    72 -> {
                        midi_updates.trySend(Pair(Control.OP2_RELEASE.code, value))
                    }


                }
            }

            if (message[0].compareTo(NOTE_ON) == 0) {
                val note = midi_notes[message[1].toInt()]!!
                midi_updates.trySend(Pair(NOTE_ON, note))
                synth_control.note_on(note, message[2].toFloat())
            } else if (message[0].compareTo(NOTE_OFF) == 0) {
                val note = midi_notes[message[1].toInt()]!!
                synth_control.note_off(note)
            }
        }
    }
}

class MainActivity : ComponentActivity(), SynthControl {
    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }

    private external fun startEngine()

    external override fun handle_change(control_num: Int, value: Float)
    external override fun note_on(frequency: Float, velocity: Float)
    external override fun note_off(frequency: Float)

    override fun arp_switch(frequency: Float) {
        if (arp_notes.contains(frequency)) {
            arp_notes.remove(frequency)
        } else {
            arp_notes.add(frequency)
        }
    }

    var arp_notes = mutableListOf<Float>()
    var midi_updates = Channel<Pair<Int, Float>>(10)

    var timer = Timer()
    var index = 0
    var midi_receiver = CustomMidiReceiver(this, midi_updates)

    override fun onCreate(savedInstanceState: Bundle?) {

        startEngine()
        super.onCreate(savedInstanceState)

        var application = this

        if (applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_MIDI)) {
            var midi_manager: MidiManager =
                applicationContext.getSystemService(Context.MIDI_SERVICE) as MidiManager

            var midi_device_handler = object : MidiManager.DeviceCallback() {
                override fun onDeviceAdded(device_info: MidiDeviceInfo?) {
                    println(device_info.toString())

                    val listener = object : MidiManager.OnDeviceOpenedListener {
                        override fun onDeviceOpened(device: MidiDevice?) {
                            println("Device opened")
                            val output_port = device?.openOutputPort(0)
                            val receiver = MidiFramer(application.midi_receiver)
                            output_port?.connect(receiver)
                        }
                    }

                    midi_manager.openDevice(device_info, listener, Handler())
                }

                override fun onDeviceRemoved(device: MidiDeviceInfo?) {
                }
            }
            midi_manager.registerDeviceCallback(
                midi_device_handler, Handler()
            )


        } else {
            println("Not available")
        }

        setContent {
            UI(this, this.midi_updates)
        }
    }
}

fun create_note_to_freq(): Pair<Map<String, Float>, Map<Int, Float>> {

    val a4_freq = 440.0f;
    val a = 2f.pow(1f / 12f);
    val c4_freq = a4_freq * a.pow(-9)

    var note_offset = -9 - 12 * 3
    var midi_index = 24

//    println("Note to freq starting ${note_offset}")

    val scale: List<String> =
        listOf("C", "d", "D", "e", "E", "F", "g", "G", "a", "A", "h", "H");
    val notes: MutableMap<String, Float> = emptyMap<String, Float>().toMutableMap()
    val midi_notes: MutableMap<Int, Float> = emptyMap<Int, Float>().toMutableMap()

    var octave = 1

    var note_index = 0
    while (note_offset < 40) {

        val note_value = "${scale[note_index]}${octave}"
        val frequency = a4_freq * a.pow(note_offset)

        notes[note_value] = frequency
        midi_notes[midi_index] = frequency

        midi_index += 1
        note_index += 1

        if (note_index == scale.size) {
            octave += 1
        }

        note_index %= scale.size
        note_offset += 1

//        println("${note_value} - ${frequency}")
    }

//    println("Note to freq done ${note_offset}")

    return Pair(notes, midi_notes);
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Keyboard(control: SynthControl) {

    val (note_to_frequency, _midi_to_frequency) = create_note_to_freq()

    val notes: List<String> =
        listOf("C4", "C'4", "D4", "D'4", "E4", "F4", "F'4", "G4", "G'4", "A4", "A'4", "H4");
    val a_freq = 440.0f;
    val a = 2f.pow(1f / 12f);

    for (octave in 3..4) {
        Row(horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
            note_to_frequency.forEach {
                val note = it.key
                val frequency = it.value
                if (note.contains(octave.toString())) {
                    Button(
                        onClick = { },
                        modifier = Modifier
                            .pointerInteropFilter {
                                when (it.action) {
                                    MotionEvent.ACTION_DOWN -> {
                                        control.note_on(frequency, 100f)
                                    }
                                    MotionEvent.ACTION_MOVE -> {}
                                    MotionEvent.ACTION_UP -> {
                                        control.note_off(frequency)
                                    }
                                }
                                true
                            }
                            .weight(1f)
                    ) {
                        Text(note)
                    }
                }
            }

        }
    }
}

fun Float.format(digits: Int) = "%.${digits}f".format(this)

@Composable
fun SliderWithText(
    text: String,
    control_num: Control,
    control: SynthControl,
    controls_state: MutableMap<Int, Float>
) {
    if (!controls_state.containsKey(control_num.code)) {
        controls_state[control_num.code] = 0f
    }
    Row(modifier = Modifier.wrapContentWidth()) {
        Text(
            "$text: ${controls_state[control_num.code]?.format(2)}", modifier = Modifier
                .padding(10.dp)
                .width(150.dp)
                .wrapContentWidth()
        )
        Slider(
            modifier = Modifier.width(180.dp),
            value = controls_state[control_num.code]!!,
            onValueChange = {
                controls_state[control_num.code] = it; control.handle_change(
                control_num.code,
                it
            )
            })
    }
}

@Composable
fun EffectControls(synth_control: SynthControl, controls_state: MutableMap<Int, Float>) {
    SliderWithText(text = "Delay", Control.DELAY, synth_control, controls_state)
    SliderWithText(text = "Reverb Mix", Control.REVERB_MIX, synth_control, controls_state)
    SliderWithText(text = "Reverb Decay", Control.REVERB_DECAY, synth_control, controls_state)
    SliderWithText(text = "Filter cutoff", Control.FILTER_CUTOFF, synth_control, controls_state)
}

@Composable
fun ADSRControls(synth_control: SynthControl, controls_state: MutableMap<Int, Float>) {

    Row() {
        Column {
            SliderWithText(text = "OP1 Attack", control_num = Control.OP1_ATTACK, synth_control, controls_state)
            SliderWithText(text = "OP1 Decay", control_num = Control.OP1_DECAY, synth_control, controls_state)
            SliderWithText(text = "OP1 Level", control_num = Control.OP1_LEVEL, synth_control, controls_state)
            SliderWithText(text = "OP1 Release", control_num = Control.OP1_RELEASE, synth_control, controls_state)
        }
        Column() {
            SliderWithText(text = "OP2 Attack", control_num = Control.OP2_ATTACK, synth_control, controls_state)
            SliderWithText(text = "OP2 Decay", control_num = Control.OP2_DECAY, synth_control, controls_state)
            SliderWithText(text = "OP2 Level", control_num = Control.OP2_LEVEL, synth_control, controls_state)
            SliderWithText(text = "OP2 Release", control_num = Control.OP2_RELEASE, synth_control, controls_state)
        }
    }
}

enum class ModulationTarget {
    VELOCITY,
    FILTER,
    REVERB,
    FM
}

@Composable
fun SynthControls(
    synth_control: SynthControl,
    controls_state: MutableMap<Int, Float>,
    midi_updates: Channel<Pair<Int, Float>>
) {
    var controls by remember { mutableStateOf(5) };
    val arp_active_notes = remember { mutableStateListOf<Boolean>(true, false, true, true) }

    var arp_notes = remember {
        mutableStateListOf(
            ArpSegment(440f, 440f, 440f, 440f, 0f),
            ArpSegment(440f, 440f, 440f, 440f, 0f),
            ArpSegment(440f, 440f, 440f, 440f, 0f),
            ArpSegment(440f, 440f, 440f, 440f, 0f),
        )
    }
    var modulation_target by remember{ mutableStateOf(ModulationTarget.VELOCITY)}
    var current_arp_segment by remember { mutableStateOf(0) }
    var current_note_index by remember { mutableStateOf(0) }
    var arp_on by remember {
        mutableStateOf(false)
    }
    var arp_delay by remember {
        mutableStateOf(400L)
    }

    var arp_segment_repeats by remember {
        mutableStateOf(4)
    }

    var arp_played_segment by remember {
        mutableStateOf(0)
    }

    val arp_player by produceState(initialValue = 0) {
        var arp_played_note = 0
        var current_segment_repeat = 0

        var current_note_frequency = 440f

        while (true) {
            kotlinx.coroutines.delay(arp_delay)
            synth_control.note_off(current_note_frequency)

            if (!arp_on) {
                arp_played_segment = 0
                arp_played_note = 0
                current_segment_repeat = 0
                continue
            }

            current_note_frequency = arp_notes[arp_played_segment].get_by_index(arp_played_note)

            var note_velocity = 100f
            val modulation = arp_notes[arp_played_segment].modulation

            when(modulation_target) {
                ModulationTarget.VELOCITY -> {note_velocity = modulation * 100f}
                ModulationTarget.FILTER -> synth_control.handle_change(3, modulation)
                ModulationTarget.REVERB -> synth_control.handle_change(2, modulation)
                ModulationTarget.FM -> synth_control.handle_change(111, modulation)
            }
            synth_control.note_on(current_note_frequency, note_velocity)

            arp_played_note++
            if (arp_played_note >= 4) {
                arp_played_note = 0
                current_segment_repeat += 1

                if (current_segment_repeat >= arp_segment_repeats) {
                    arp_played_segment += 1
                    arp_played_segment %= arp_notes.size
                    current_segment_repeat = 0
                }
            }
        }
    }

    val midi_update_listener by produceState(initialValue = 0) {

        while (true) {
            delay(1)
            val update = midi_updates.receive()

            if (update.first == NOTE_ON) {

                if (current_note_index >= arp_notes.size) {
                    continue
                }

                var new_note = arp_notes[current_arp_segment].copy()
                new_note.set_by_index(current_note_index, update.second)
                arp_notes[current_arp_segment] = new_note

                current_note_index += 1

            } else {
                controls_state[update.first] = update.second;
                synth_control.handle_change(
                    update.first,
                    update.second
                );
            }

        }
    }



    TopAppBar() {
        Row() {
            Button(onClick = { controls = 1 }) {
                Text(text = "ADSR")
            }

            Button(onClick = { controls = 3 }) {
                Text(text = "FM")
            }

            Button(onClick = { controls = 4 }) {
                Text(text = "EFFECTS")
            }

            Button(onClick = { controls = 5 }) {
                Text(text = "ARP")
            }

            Button(onClick = { arp_on = !arp_on }) {
                if (arp_on) {
                    Text(text = "Stop")
                } else {
                    Text(text = "Play")
                }
            }
        }
    }

    when (controls) {
        1 -> {
            ADSRControls(synth_control, controls_state)
        }
        3 -> {
            FMControls(synth_control, controls_state)
        }
        4 -> {
            EffectControls(synth_control, controls_state)
        }
        5 -> {
            ArpControls(
                synth_control,
                controls_state,
                arp_notes,
                current_arp_segment,
                {
                    current_arp_segment = it
                    current_note_index = 0
                },
                arp_current_played_segment = arp_played_segment,
                arp_delay,
                { arp_delay = it },
                arp_segment_repeats
            , {
                arp_segment_repeats = it.toInt()
            },
                modulation_target,
                {modulation_target = it}
            )
        }
    }
}

@Parcelize
data class ArpSegment(
    var note_0: Float,
    var note_1: Float,
    var note_2: Float,
    var note_3: Float,
    var modulation: Float,
//    val enabled: Boolean
) :
    Parcelable {
    fun get_by_index(index: Int): Float {
        when (index) {
            0 -> {
                return note_0
            }
            1 -> {
                return note_1
            }
            2 -> {
                return note_2
            }
            else -> return note_3
        }
    }

    fun set_by_index(index: Int, value: Float) {
        when (index) {
            0 -> {
                note_0 = value
            }
            1 -> {
                note_1 = value
            }
            2 -> {
                note_2 = value
            }
            else -> note_3 = value
        }
    }

    override fun toString(): String {
        return "%.2f".format(note_0) + "\n" + "%.2f".format(note_1) + "\n" + "%.2f".format(note_2) + "\n" + "%.2f".format(
            note_3
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ArpControls(
    synth_control: SynthControl,
    controls_state: MutableMap<Int, Float>,
    arp_notes: MutableList<ArpSegment>,
    current_arp_segment: Int,
    on_arp_segment_changed: (Int) -> Unit,
    arp_current_played_segment: Int,
    arp_speed: Long,
    on_arp_speed_change: (Long) -> Unit,

    arp_repetitions: Int,
    on_arp_repetitions_change: (Long) -> Unit,

    modulation_target: ModulationTarget,
    on_modulation_target_change: (ModulationTarget) -> Unit,
) {
    var arp_float by remember { mutableStateOf(arp_speed.toFloat()) }
    var arp_repetitions_float by remember { mutableStateOf(arp_repetitions.toFloat()) }

    Row {

        Text(text = "Arp speed", modifier = Modifier.weight(1f))
        Slider(
            value = arp_float,
            onValueChange = { arp_float = it; on_arp_speed_change(((it + 1) * 100).toLong()) },
            modifier = Modifier.weight(1f)
        )

        Text(text = "Arp repetitions", modifier = Modifier.weight(1f))
        Slider(
            value = arp_repetitions_float,
            steps = 8,
            onValueChange = {
                arp_repetitions_float = it; on_arp_repetitions_change(((it * 10 + 1).toLong()))
            },
            modifier = Modifier.weight(1f)
        )
    }
    Row {
        for (i in 0..arp_notes.size - 1) {
            val alpha = if (i == current_arp_segment) 1.0f else 0.4f

            Button(
                colors = ButtonDefaults.buttonColors(
                    backgroundColor =
                    if (arp_current_played_segment == i) Color.White else Color.Blue
                ),
                onClick = { on_arp_segment_changed(i) },
                modifier = Modifier
                    .weight(1f)


            ) {
                Text(text = arp_notes[i].toString(), modifier = Modifier.alpha(alpha))
                Slider(
                    value = arp_notes[i].modulation,
                    onValueChange = {
                        var new_note = arp_notes[i].copy(); new_note.modulation = it; arp_notes[i] =
                        new_note
                    })
            }

        }
    }

    Row {
        for(target in ModulationTarget.values()) {
            val alpha = if(modulation_target == target) 1.0f else 0.5f
            Button(onClick = { on_modulation_target_change(target) }) {
                Text(text = target.toString(), modifier = Modifier.alpha(alpha))
            }
        }
    }

    Spacer(modifier = Modifier.padding(vertical = 1.dp))
}

@Composable
fun FMControls(synth_control: SynthControl, controls_state: MutableMap<Int, Float>) {
    Row {
        Column() {
            SliderWithText(text = "OP2 Freq", Control.OP2_FREQ, synth_control, controls_state)
            SliderWithText(text = "OP3 Freq", Control.OP3_FREQ, synth_control, controls_state)
        }

        Column() {
            SliderWithText(text = "OP2 Strength", Control.OP2_STRENGTH, synth_control, controls_state)
            SliderWithText(text = "OP3 Strength", Control.OP3_STRENGTH, synth_control, controls_state)
        }
    }
}

@Composable
fun UI(synth_control: SynthControl, midi_updates: Channel<Pair<Int, Float>>) {
    val controls_state = remember { mutableStateMapOf<Int, Float>() }

    MyApplicationComposeTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color.White
        ) {
            Column() {
                SynthControls(synth_control, controls_state, midi_updates)
                Keyboard(synth_control)
            }
        }
    }
}

@Preview(showBackground = true, device = Devices.AUTOMOTIVE_1024p, widthDp = 720, heightDp = 360)
@Composable
fun DefaultPreview() {
    val midi_updates = Channel<Pair<Int, Float>>();
    val synth_control = SynthControlMock();
    UI(synth_control, midi_updates)
}