import mido
import socket
import struct

UDP_IP = "127.0.0.1"
UDP_PORT = 8081

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

inputs = mido.get_input_names()
valid_inputs = [input for input in inputs if "Arturia" in input or "Digitakt" in input]
if not any(valid_inputs):
    print("No keyboard detected:", inputs)
    exit(1)

msg_types = {
    "note_on": 1,
    "note_off": 0,
    "control_change": 2,
    # ""
}

with mido.open_input(valid_inputs[0]) as inport:
    for msg in inport:
        if msg.type == "clock":
            continue
        print(vars(msg))

        msg_type = msg_types.get(msg.type, None)
        if msg_type is None:
            continue

        if "note" in msg.type:
            note_value = msg.note
            velocity = msg.velocity
            if velocity == 0:
                msg_type = msg_types["note_off"]
            print(msg_type, note_value, velocity)
            data = struct.pack("III", msg_type, note_value, velocity)
        else:
            control = msg.control
            value = msg.value
            data = struct.pack("III", msg_type, control, value)
        # print(vars(msg))
        sock.sendto(data, (UDP_IP, UDP_PORT))

