#pragma once
#include "operator.h"
#include <cstdint>
#include <vector>

class Operators {

public:
  Operator A, B, C; //, D;
  std::vector<Operator *>
      operators; // Used for setting parameters for all operators

public:
  Operators() {
    this->operators.push_back(&A);
    this->operators.push_back(&B);
    this->operators.push_back(&C);
//    this->operators.push_back(&D);
  }
  void set_sample_rate(int32_t sample_rate) {
    // A.set_sample_rate(sample_rate);
    for (auto &op : this->operators) {
      op->set_sample_rate(sample_rate);
    }
  }

  void note_on(float freq, float velocity) {
    for (auto &op : this->operators) {
      op->note_on(freq, velocity);
    }
  }

  Operator &by_index(int index) {
    if (index == 0) {
        return A;
    }
    else if (index == 1) {
      return B;
   } else { // if (index == 3) {
      return C;
    }
//    return D;
  }

  void note_off() {
    for (auto &op : this->operators) {
      op->note_off();
    }
  }
};

class Algorithm {
public:
  virtual void render(float *samples, int32_t num_of_samples,
                      Operators &renderer) = 0;
};

class Algo1 : public Algorithm {
public:
  void render(float *samples, int32_t num_of_samples, Operators &renderer) {
    for (int i = 0; i < num_of_samples; i++) {

      samples[i] +=
          renderer.A.tick(renderer.B.tick(renderer.C.tick(0) ) );
    }
  }
};

class Algo2 : public Algorithm {
  void render(float *samples, int32_t num_of_samples, Operators &o) {
    for (int i = 0; i < num_of_samples; i++) {
      samples[i] += o.A.tick(0);
    }
  }
};
