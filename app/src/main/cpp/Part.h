#pragma once

// #include "Butterworth.h"
// #include "Butterworth.h"
#include "Freeverb.h"
#include "MVerb.h"
#include "algorithms.h"
#include "operator.h"

#include <algorithm>
#include <cstdint>
#include <deque>

//#ifdef __ANDROID__

// #include <android/log.h>

//#else
//#define ANDROID_LOG_ERROR 0
//void __android_log_print(int a, std::string b, std::string c) {}
//#endif

class Filter {
    float last_value = 0;
    float strength = 0;
    float one_minus_strength = 1;

public:
    void set_strength(float strength) {
        this->strength = strength;
        this->one_minus_strength = 1.0 - strength;
    }

    float feed(float value) {
        this->last_value = this->last_value * strength + value * one_minus_strength;
        return this->last_value;
    }
};

class Voice {
public:
    // Operator op;
    Operators o;
    Algorithm *a;
    Algorithm **algorithms;

    int algo_index = 0;

    float frequency = 0;

    Voice() {
        algorithms = new Algorithm *[2];
        algorithms[0] = new Algo1();
        algorithms[1] = new Algo2();
        a = algorithms[algo_index];
    }

    void switch_algo() { a = new Algo2(); }

    void note_on(float frequency, int32_t velocity) {
        this->frequency = frequency;
        this->o.note_on(frequency, velocity);
    }

    void note_off() {
        std::cout << this->frequency << " voice down" << std::endl;
        this->o.note_off();
    }

    void render(float *samples, int32_t num_of_samples) {
        a->render(samples, num_of_samples, o);
    }
};

class Delay {
    DelayLine delay_line;
    float wet;
    float dry;

public:
    Delay(int sample_rate) : delay_line(sample_rate) { this->set_wet(0.5); }

    void set_wet(float wet) {
        this->wet = wet;
        this->dry = 1.0 - wet;
    }

    float tick(float sample) {
        float output = sample + this->delay_line.read() * this->wet;
        this->delay_line.write_and_advance(output);
        return output;
    }
};

class Part {
public:
    std::vector<Voice> voices;
    int voice_index = 0;
    Freeverb *freeverb;
    MVerb<float> *mverb;
    // DelayLine *delay_line;
    Delay *delay;
    Filter filter;

public:
    Part() : voices(16) {}

    void handle_change(int32_t control_num, float value) {

        if (control_num >= 100) {
            int operator_num = (control_num - 100) / 10;
//            int control_num = control_num - 100 - (operator_num * 10);
            control_num -= 100;
//            __android_log_print(ANDROID_LOG_ERROR, "xd", "Operator %d control %d", operator_num, control_num);
            control_num -= operator_num * 10;
//            __android_log_print(ANDROID_LOG_ERROR, "xd", "Operator %d control %d", operator_num, control_num);

            switch (control_num) {
                case 1:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).setStrength(value * 10);
                    }
                    break;
                case 2:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).setFrequencyMultiplier(value * 10);
                    }
                    break;
                case 3:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).adsr.set_attack(value);
                    }
                    break;
                case 4:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).adsr.set_decay_time(value);
                    }
                    break;

                case 5:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).adsr.set_decay_level(value);
                    }
                    break;
                case 6:
                    for (auto &voice : this->voices) {
                        voice.o.by_index(operator_num).adsr.set_release(value * 2);
                    }
                    break;

                default:
                    break;
            }

            return;
        }

        switch (control_num) {
            case 0:
                delay->set_wet(value);
                break;
            case 1:
                this->mverb->setParameter(MVerb<float>::MIX, value);
                break;
            case 2:
                this->mverb->setParameter(MVerb<float>::DECAY, value);
                break;
            case 3:
                std::cout << "Filter" << std::endl;
                this->filter.set_strength(value);
                break;
            default:
                break;
        }
    }

    void setSampleRate(int32_t sample_rate) {
        for (auto &v : this->voices) {
            v.o.set_sample_rate(sample_rate);
        }
        this->freeverb = new Freeverb(sample_rate);
        // this->delay_line = new DelayLine(sample_rate / 5);
        this->delay = new Delay(sample_rate / 2);
        this->mverb = new MVerb<float>();
        this->mverb->setSampleRate(sample_rate);
        this->mverb->setParameter(MVerb<float>::PREDELAY, 0.0f);
        this->mverb->setParameter(MVerb<float>::DECAY, 0.1f);
        this->mverb->setParameter(MVerb<float>::MIX, 0.0f);
        this->delay->set_wet(0.0);
        // this->mverb->setParameter(MVerb<float>::DECAY, 0.1f);
        // this->butter.design(2);
    }

    void switch_algo() {
        for (auto &voice : this->voices) {
            voice.switch_algo();
        }
    }

    void render(float *audioData, int32_t numFrames) {

        std::fill(audioData, audioData + numFrames, 0);
        for (auto &v : this->voices) {
            v.render(audioData, numFrames); // Todo: hardcode no modulation now
        }

        // this->butter.process();
        // float **inputs = ;

        for (int i = 0; i < numFrames; i++) {
            // float sample = audioData[i];
            // audioData[i] = audioData[i] * 0.5 + this->delay_line->read() * .5;
            // this->delay_line->write_and_advance(audioData[i]);
            audioData[i] = this->filter.feed(audioData[i]);
            audioData[i] = this->delay->tick(audioData[i]);
        }

        this->mverb->process(audioData, audioData, numFrames);
        // for (int i = 0; i < numFrames; i++) {
        //   float input[2] = {audioData[i], 0.0};
        // audioData[i] = this->freeverb->tick(input).first;
        // }
    }

    void note_on(float frequency, int32_t velocity) {
        this->voices[voice_index].note_on(frequency, velocity);

        voice_index++;
        voice_index %= this->voices.size();
    }

    void note_off(float frequency) {
        for (auto &voice : this->voices) {
            if (voice.frequency == frequency) {
                voice.note_off();
            }
        }
    }
};
