#include <utility>
#include <vector>

class DelayLine {
  std::vector<float> buffer;
  int index = 0;

public:
  DelayLine(int size) : buffer(size) {}

  float read() { return this->buffer[index]; }

  void write_and_advance(float value) {
    this->buffer[this->index] = value;

    if (this->index == this->buffer.size() - 1) {
      this->index = 0;
    } else {
      this->index++;
    }
  }
};

class AllPass {
  DelayLine delay_line;

public:
  AllPass(int delay_length) : delay_line(delay_length) {}

  float tick(float input) {
    float delayed = this->delay_line.read();
    float output = -input + delayed;
    float feedback = 0.5;
    this->delay_line.write_and_advance(input + delayed * feedback);

    return output;
  }
};

class CombFilter {
  DelayLine delay_line;
  float feedback;
  float filter_state;
  float dampening;
  float dampening_inverse;

public:
  CombFilter(int delay_length) : delay_line(delay_length) {
    this->feedback = 0.5;
    this->filter_state = 0;
    this->dampening = 0.5;
    this->dampening_inverse = 0.5;
  }

  void set_feedback(float feedback) { this->feedback = feedback; }

  void set_dampening(float dampening) {
    this->dampening = dampening;
    this->dampening_inverse = 1.0 - dampening;
  }

  float tick(float input) {
    float output = this->delay_line.read();
    this->filter_state =
        output * dampening_inverse + this->filter_state * dampening;
    this->delay_line.write_and_advance(input +
                                       this->filter_state * this->feedback);

    return output;
  }
};

const float FIXED_GAIN = 0.015;

const float SCALE_WET = 3.0;
const float SCALE_DAMPENING = 0.4;

const float SCALE_ROOM = 0.28;
const float OFFSET_ROOM = 0.7;

const int STEREO_SPREAD = 23;

const int COMB_TUNING_L1 = 1116;
const int COMB_TUNING_R1 = 1116 + STEREO_SPREAD;
const int COMB_TUNING_L2 = 1188;
const int COMB_TUNING_R2 = 1188 + STEREO_SPREAD;
const int COMB_TUNING_L3 = 1277;
const int COMB_TUNING_R3 = 1277 + STEREO_SPREAD;
const int COMB_TUNING_L4 = 1356;
const int COMB_TUNING_R4 = 1356 + STEREO_SPREAD;
const int COMB_TUNING_L5 = 1422;
const int COMB_TUNING_R5 = 1422 + STEREO_SPREAD;
const int COMB_TUNING_L6 = 1491;
const int COMB_TUNING_R6 = 1491 + STEREO_SPREAD;
const int COMB_TUNING_L7 = 1557;
const int COMB_TUNING_R7 = 1557 + STEREO_SPREAD;
const int COMB_TUNING_L8 = 1617;
const int COMB_TUNING_R8 = 1617 + STEREO_SPREAD;

const int ALLPASS_TUNING_L1 = 556;
const int ALLPASS_TUNING_R1 = 556 + STEREO_SPREAD;
const int ALLPASS_TUNING_L2 = 441;
const int ALLPASS_TUNING_R2 = 441 + STEREO_SPREAD;
const int ALLPASS_TUNING_L3 = 341;
const int ALLPASS_TUNING_R3 = 341 + STEREO_SPREAD;
const int ALLPASS_TUNING_L4 = 225;
const int ALLPASS_TUNING_R4 = 225 + STEREO_SPREAD;

class Freeverb {
  CombFilter combs[8][2];
  AllPass all_passes[4][2];
  float wet_gains[2] = {0.0, 0.0};
  float wet = .5;
  float dry = .5;
  float input_gain = .0;
  float width = 0;
  float dampening = 0;
  float room_size = 0;
  bool frozen = false;

  static int adjust_length(int length, int sr) {
    return int(float(length) * float(sr) / 44100.0);
  }

public:
  Freeverb(int sr)
      : combs{
            {adjust_length(COMB_TUNING_L1, sr),
             adjust_length(COMB_TUNING_R1, sr)},
            {adjust_length(COMB_TUNING_L2, sr),
             adjust_length(COMB_TUNING_R2, sr)},
            {adjust_length(COMB_TUNING_L3, sr),
             adjust_length(COMB_TUNING_R3, sr)},
            {adjust_length(COMB_TUNING_L4, sr),
             adjust_length(COMB_TUNING_R4, sr)},

            {adjust_length(COMB_TUNING_L5, sr),
             adjust_length(COMB_TUNING_R5, sr)},
            {adjust_length(COMB_TUNING_L6, sr),
             adjust_length(COMB_TUNING_R6, sr)},
            {adjust_length(COMB_TUNING_L7, sr),
             adjust_length(COMB_TUNING_R7, sr)},
            {adjust_length(COMB_TUNING_L8, sr),
             adjust_length(COMB_TUNING_R8, sr)},

        }, all_passes{
          {
          adjust_length(ALLPASS_TUNING_L1, sr),
          adjust_length(ALLPASS_TUNING_R1, sr),
          },
  {
          adjust_length(ALLPASS_TUNING_L2, sr),
          adjust_length(ALLPASS_TUNING_R2, sr),
          },

  {
          adjust_length(ALLPASS_TUNING_L3, sr),
          adjust_length(ALLPASS_TUNING_R3, sr),
          },
  {
          adjust_length(ALLPASS_TUNING_L4, sr),
          adjust_length(ALLPASS_TUNING_R4, sr),
          },

        } {

    this->set_wet(.1);
    this->set_width(0.0);
    this->set_dampening(0.5);
    this->set_room_size(.5);
    this->set_frozen(false);
  }

  void set_wet(float value) {
    this->wet = value * SCALE_WET;
    this->update_wet_gains();
  }

  void set_width(float value) {
    this->width = value;
    this->update_wet_gains();
  }

  void update_wet_gains() {
    this->wet_gains[0] = this->wet * (this->width / 2.0 + 0.5);
    this->wet_gains[1] = ((1.0 - this->width) / 2.0);
  }

  void set_frozen(bool value) {
    this->frozen = value;
    this->input_gain = frozen ? 0.0 : 1.0;
    this->update_combs();
  }

  void set_room_size(float value) {
    this->room_size = value * SCALE_ROOM + OFFSET_ROOM;
    this->update_combs();
  }

  void set_dampening(float value) {
    this->dampening = value * SCALE_DAMPENING;
    this->update_combs();
  }

  void update_combs() {
    float feedback = 1.0;
    float dampening = 0.0;

    if (not this->frozen) {
      feedback = this->room_size;
      dampening = this->dampening;
    }

    for (auto &comb_pair : this->combs) {
      comb_pair[0].set_feedback(feedback);
      comb_pair[1].set_feedback(feedback);

      comb_pair[0].set_dampening(dampening);
      comb_pair[1].set_dampening(dampening);
    }
  }

  std::pair<float, float> tick(float input[2]) {

    float output[2] = {0, 0};
    float input_mixed = input[0] + input[1] * FIXED_GAIN * this->input_gain;

    for (auto &comb_pair : this->combs) {
      output[0] = comb_pair[0].tick(input[0]);
      output[1] = comb_pair[1].tick(input[1]);
    }

    for (auto &allpass_pair : this->all_passes) {
      output[0] = allpass_pair[0].tick(output[0]);
      output[1] = allpass_pair[1].tick(output[1]);
    }

    return std::make_pair(
        output[0] * this->wet_gains[0] + output[1] * this->wet_gains[1] +
            input[0] * this->dry,
        output[1] * this->wet_gains[1] + output[0] * this->wet_gains[1] +
            input[1] * this->dry);
  }
};
