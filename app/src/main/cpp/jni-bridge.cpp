/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <jni.h>
#include <android/input.h>
#include "AudioEngine.h"

static AudioEngine audioEngine = AudioEngine();

extern "C" {

JNIEXPORT void JNICALL
Java_com_example_myapplicationcompose_MainActivity_note_1on(JNIEnv *env, jobject obj,
                                                            jfloat frequency, jfloat velocity) {
    audioEngine.part.note_on(frequency, velocity);
}

JNIEXPORT void JNICALL
Java_com_example_myapplicationcompose_MainActivity_note_1off(JNIEnv *env, jobject obj,
                                                             jfloat frequency) {
    audioEngine.part.note_off(frequency);
}

JNIEXPORT void JNICALL
Java_com_example_myapplicationcompose_MainActivity_handle_1change(JNIEnv *env, jobject obj,
                                                                 jint change_num, jfloat value) {
    audioEngine.part.handle_change(change_num, value);
}

JNIEXPORT void JNICALL
Java_com_example_myapplicationcompose_MainActivity_startEngine(JNIEnv *env, jobject /* this */) {
    audioEngine.start();
}


JNIEXPORT void JNICALL
Java_com_example_wavemaker_MainActivity_setFM(JNIEnv *env, jobject
obj,
                                              float frequency,
                                              float mod
) {
    audioEngine.
            setFM(frequency, mod
    );
}


JNIEXPORT void JNICALL
Java_com_example_wavemaker_MainActivity_stopEngine(JNIEnv *env, jobject /* this */) {
    audioEngine.stop();
}

}
