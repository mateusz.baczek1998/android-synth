#include <SFML/Window/Keyboard.hpp>
#include <string>

enum class ControlMode { NORMAL, MODULATION, ENVELOPES };

class DesktopControl {
  ControlMode control_mode = ControlMode::NORMAL;
  int current_voice = 0;

public:
  std::string get_current_voice() { return std::to_string(current_voice); }
  std::string get_current_mode() {
    switch (control_mode) {
    case ControlMode::NORMAL:
      return "Normal";
    case ControlMode::MODULATION:
      return "Modulation";
    case ControlMode::ENVELOPES:
      return "Envelopes";
    default:
      return "You messed up m8";
    }
  }

  void process_keypress(sf::Keyboard::Key key) {
    if (key == sf::Keyboard::Key::Escape) {
      control_mode = ControlMode::NORMAL;
      return;
    }

    if (control_mode == ControlMode::NORMAL) {

      if (key == sf::Keyboard::Keyboard::E) {
        this->control_mode = ControlMode::ENVELOPES;
      }

      if (key == sf::Keyboard::Keyboard::M) {
        this->control_mode = ControlMode::MODULATION;
      }
      if (key == sf::Keyboard::Num0) {
        current_voice = 0;
      }

      if (key == sf::Keyboard::Num1) {
        current_voice = 1;
      }

      if (key == sf::Keyboard::Num2) {
        current_voice = 2;
      }
    }

    if (control_mode == ControlMode::ENVELOPES) {
    }

    if (control_mode == ControlMode::MODULATION) {
    }
  }
};
