// #include "Oscillator.h"
#include "Part.h"
#include "desktop_control.h"
#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Network/UdpSocket.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <deque>
#include <functional>
#include <iostream>
#include <map>
#include <math.h>
#include <numbers>
#include <optional>
#include <ostream>
#include <thread>
#include <utility>
#include <vector>

const unsigned int SAMPLE_RATE = 44100;
const unsigned int BUFFER_SIZE = 1000;
const unsigned int PORT = 8081;

typedef struct {
  int32_t note_on;
  int32_t note;
  int32_t velocity;
} __attribute__((packed)) MidiNote;

void print(MidiNote &note) {
  std::cout << note.note_on << " " << note.note << " " << note.velocity
            << std::endl;
}

class MyAudioStream : public sf::SoundStream {

  std::vector<float> float_samples;

public:
  std::vector<sf::Int16> samples;
  Part part;

public:
  MyAudioStream() {
    this->part.setSampleRate(SAMPLE_RATE);

    for (int i = 0; i < BUFFER_SIZE; i++) {
      samples.push_back(0);
      float_samples.push_back(0);
    }
    initialize(1, SAMPLE_RATE);
  }

  virtual bool onGetData(Chunk &data) {
    data.sampleCount = BUFFER_SIZE;

    std::fill(this->float_samples.begin(), this->float_samples.end(), 0);

    this->part.render(&float_samples[0], BUFFER_SIZE);

    for (int i = 0; i < BUFFER_SIZE; i++) {
      this->samples[i] = this->float_samples[i] * 32767.0f;
    }

    data.samples = &this->samples[0];
    return true;
  }

  virtual void onSeek(sf::Time timeOffset) {
    std::cout << "OnSeek" << std::endl;
    // std::cout << timeOffset << std::endl;
  }
};

// std::optional<std::string> get_char_from_event(sf::Event event) {
std::optional<char> get_char_from_event(sf::Event event) {

  const sf::Keyboard::Key keycode = event.key.code;
  if (keycode >= sf::Keyboard::A && keycode <= sf::Keyboard::Z) {
    char chr = static_cast<char>(keycode - sf::Keyboard::A + 'A');
    // return std::optional<std::string>{chr};
    return std::optional<char>{chr};
  }

  return std::nullopt;
}

class NoteOffEvent {
public:
  float frequency;
  int milliseconds;
};

class NoteOffTriggerer {
  sf::Clock clock;
  std::deque<NoteOffEvent> note_off_queue;

public:
  NoteOffTriggerer() { clock.restart(); }

  void add_trigger(NoteOffEvent event) { note_off_queue.push_back(event); }

  void tick(MyAudioStream &audio_stream) {
    int elapsed = clock.getElapsedTime().asMilliseconds();
    // std::cout << note_off_queue.size() << std::endl;

    for (auto &note_off_event : note_off_queue) {
      note_off_event.milliseconds -= elapsed;
      if (note_off_event.milliseconds <= 0) {
        audio_stream.part.note_off(note_off_event.frequency);
      }
    }
    // std::cout << note_off_queue.size() << std::endl;

    // note_off_queue.erase_if();
    std::erase_if(note_off_queue,
                  [](NoteOffEvent e) { return e.milliseconds <= 0; });
  }
};

int main() {

  DesktopControl desktop_control;
  auto background_color = sf::Color(38, 50, 56);

  NoteOffTriggerer note_off_triggerer;

  const float two_power_1_by_12 = std::pow(2, 1.0 / 12.0);
  float f_0 = 440.0; // A4
  int a4_midi_num = 69;

  std::map<std::pair<char, char>, float> notes;
  std::map<int, float> midi_notes;

  for (int i = 10; i < 127; i++) {
    int note_delta_steps = i - a4_midi_num;
    float note_frequency = f_0 * pow(two_power_1_by_12, note_delta_steps);
    midi_notes[i] = note_frequency;
  }

  char major_scale[] = {'C', 'd', 'D', 'e', 'E', 'F', 'g', 'G', 'a', 'A', 'h'};

  int current_octave = 4; // Starting from h4;

  int note_delta_steps = 1;

  while (current_octave > 2) {
    int current_note = 10; // Starting from h;
    while (current_note >= 0) {
      float note_frequency = f_0 * pow(two_power_1_by_12, note_delta_steps);

      notes[std::make_pair(major_scale[current_note], current_octave + 48)] =
          note_frequency;

      std::cout << major_scale[current_note] << " " << current_octave << " - "
                << note_frequency << std::endl;

      current_note--;
      note_delta_steps--;
    }
    current_octave--;
  }

  MyAudioStream audio_stream;

  // sf::SoundBuffer buffer;
  // sf::Sound sound;

  MidiNote note;

  sf::UdpSocket socket;
  sf::UdpSocket orca_socket;
  socket.setBlocking(false);
  orca_socket.setBlocking(false);
  if (socket.bind(PORT) != sf::Socket::Done) {
    std::cerr << "Unable to bind to port " << PORT << std::endl;
    return 1;
  }

  if (orca_socket.bind(49161) != sf::Socket::Done) {
    std::cerr << "Unable to bind to port " << 49161 << std::endl;
    return 1;
  }

  // if (!buffer.loadFromFile("sound.wav")) {
  //   return -1;
  // }

  // sound.setBuffer(buffer);
  // sound.play();
  audio_stream.play();
  std::cout << "audio_stream playing" << std::endl;

  std::deque<NoteOffEvent> note_off_queue;

  char data[100];
  sf::IpAddress sender;
  std::size_t received;
  unsigned short port;

  sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
  window.setKeyRepeatEnabled(false);
  window.setFramerateLimit(30); // call it once, after creating the window

  float fm_freq = 0;
  float fm_strength = 0;
  float delay_strength = 0;
  float mouse_y_proc = 0.0;

  sf::Keyboard::Key current_key;

  sf::Font font;
  font.loadFromFile("/usr/share/fonts/noto/NotoSansMono-Regular.ttf");
  sf::Text text;

  text.setFont(font); // font is a sf::Font
  text.setString("Hello world");
  text.setCharacterSize(24); // in pixels, not points!
  text.setFillColor(sf::Color::Red);
  while (window.isOpen()) {
    // Keyboard input
    text.setString(desktop_control.get_current_mode() + " - " +
                   desktop_control.get_current_voice());
    sf::Event event;
    note_off_triggerer.tick(audio_stream);

    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
      } else if (event.type == sf::Event::MouseMoved) {
        mouse_y_proc =
            1.0 - (float)event.mouseMove.y / (float)window.getSize().y;

        if (mouse_y_proc < 0) {
          mouse_y_proc = 0;
        }
        if (mouse_y_proc > 1.0) {
          mouse_y_proc = 1.0;
        }

        switch (current_key) {
        case sf::Keyboard::Num1:
          fm_strength = mouse_y_proc * 10.0;
          for (auto &voice : audio_stream.part.voices) {
            voice.o.B.setStrength(fm_strength);
          }
          std::cout << "s " << fm_strength << std::endl;
          break;

        case sf::Keyboard::Num2:
          fm_freq = int(mouse_y_proc * 10.0) / 4.0;
          for (auto &voice : audio_stream.part.voices) {
            voice.o.B.setFrequencyMultiplier(fm_freq);
          }

          std::cout << "f " << fm_freq << std::endl;
          break;

        case sf::Keyboard::Num3:
          delay_strength = mouse_y_proc;
          audio_stream.part.delay->set_wet(delay_strength);
          std::cout << "d " << delay_strength << std::endl;
          break;
        case sf::Keyboard::Num4:
          audio_stream.part.mverb->setParameter(MVerb<float>::MIX,
                                                mouse_y_proc);
          std::cout << "r " << mouse_y_proc << std::endl;
          break;

        case sf::Keyboard::Num5:
          audio_stream.part.filter.set_strength(mouse_y_proc);
          std::cout << "filter " << mouse_y_proc << std::endl;
          break;
        default:
          break;
        }

      } else if (event.type == sf::Event::KeyPressed) {

        desktop_control.process_keypress(event.key.code);

        if (auto note_char = get_char_from_event(event)) {

          auto frequency = notes.find(std::make_pair(*note_char, '4'));
          if (frequency != notes.end()) {
            std::cout << frequency->second << std::endl;
            audio_stream.part.note_on(frequency->second, 100.0f);
            // note_off_triggerer.add_trigger(
            // NoteOffEvent(frequency->second, 100000));
          } else {
            std::cout << "note not found" << std::endl;
          }
        } else {
          // audio_stream.part.switch_algo();
          current_key = event.key.code;
        }
      } else if (event.type == sf::Event::KeyReleased) {

        if (auto note_char = get_char_from_event(event)) {
          auto frequency = notes.find(std::make_pair(*note_char, '4'));
          if (frequency != notes.end()) {
            std::cout << frequency->second << std::endl;
            audio_stream.part.note_off(frequency->second);
          } else {
            std::cout << "note not found" << std::endl;
          }
        } else {
          std::cout << "Erasing" << std::endl;
          current_key = sf::Keyboard::Key::Comma;
        }
      }
    }

    // UDP input orca

    // 03C - part num, octave, sound
    while (orca_socket.receive(data, 100, received, sender, port) ==
           sf::Socket::Done) {
      if (received < 3) {
        continue;
      }

      char part_num = data[0];
      char octave = data[1];
      char sound = data[2];

      std::cout << "p " << part_num << " o " << octave << " s " << sound
                << std::endl;

      int note = (octave - 48) * 12;
      if (sound == 'd') {
        note += 2;
      }

      std::cout << note << std::endl;
      if (midi_notes.find(note) != midi_notes.end()) {
        float frequency = midi_notes[note];

        audio_stream.part.note_on(frequency, 100.0f);
        note_off_triggerer.add_trigger(NoteOffEvent(frequency, 100));
      }
    }

    // UDP input midi
    while (socket.receive(data, 100, received, sender, port) ==
           sf::Socket::Done) {
      if (received == sizeof(note)) {
        std::memcpy(&note, data, sizeof(note));
        std::cout << "printing note: ";
        print(note);

        if (note.note_on == 2) {
          if (note.note == 75) {
            fm_freq = float(note.velocity) / 10;
            if (fm_freq > 10.0) {
              fm_freq = 10.0;
            }
          } else if (note.note == 73) {
            fm_strength = (float)note.velocity / 10.0;
          }

          std::cout << "freq " << fm_freq << " stren " << fm_strength
                    << std::endl;
          // audio_stream.part.setFm(fm_freq, fm_strength);
        }

        if (midi_notes.find(note.note) != midi_notes.end()) {
          float frequency = midi_notes[note.note];

          if (note.note_on == 1) {
            audio_stream.part.note_on(frequency, note.velocity);

          } else {

            audio_stream.part.note_off(frequency);
          }
        }
        // audio_stream.part.note_on(220);
      }
    }

    if (received != 0)
      std::cout << received << " " << sizeof(note) << std::endl;

    window.clear(background_color);

    text.setPosition(0, window.getSize().y - text.getCharacterSize() - 5);
    window.draw(text);
    auto window_size = window.getSize();
    float sample_width = window_size.x / audio_stream.samples.size() * 2;
    for (float i = 0; i < audio_stream.samples.size() - 1; i++) {
      float percent = i / audio_stream.samples.size();

      sf::Vertex line[] = {
          sf::Vertex(
              sf::Vector2f(percent * window_size.x,
                           audio_stream.samples[i] * 0.03 + window_size.y / 2)),
          sf::Vertex(sf::Vector2f(percent * window_size.x + sample_width,
                                  audio_stream.samples[i + 1] * 0.03 +
                                      window_size.y / 2))};

      window.draw(line, 2, sf::Lines);
    }

    window.display();
  }

  return 0;

  while (1) {
    if (socket.receive(data, 100, received, sender, port) != sf::Socket::Done) {
      std::cerr << "UDP receive error occured" << std::endl;
      return 0;
    }

    if (received < 3) {
      continue;
    }

    auto frequency = notes.find(std::make_pair(data[1], data[2]));
    if (frequency != notes.end()) {
      std::cout << frequency->second << std::endl;
      // audio_stream.o1.note_on(frequency->second);
    } else {
      // audio_stream.o1.note_off();
    }

    for (int i = 0; i < received; i++) {
      std::cout << data[i];
    }
    // std::cout << std::endl;
  }
  return 0;
}
