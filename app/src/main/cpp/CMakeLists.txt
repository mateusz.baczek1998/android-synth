cmake_minimum_required(VERSION 3.10)

# set the project name
project(SfmlAudio)

# Find SFML shared libraries
find_package(
  SFML 2.5
  COMPONENTS
  system window graphics network audio REQUIRED
)

find_library(FSPFilters shared)

add_executable(SfmlAudio main.cpp)
set_property(TARGET SfmlAudio PROPERTY CXX_STANDARD 20)

target_include_directories(SfmlAudio PUBLIC shared)
target_link_libraries(SfmlAudio PRIVATE sfml-audio sfml-network sfml-graphics)
