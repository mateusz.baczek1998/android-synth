/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PART1_OSCILLATOR_H
#define PART1_OSCILLATOR_H

#include <atomic>
#include <cstdint>
#include <iostream>
#include <math.h>
#include <random>
#include <stdint.h>
#include <vector>

#define PI 3.14159
#define TWO_PI (PI * 2)
#define AMPLITUDE 0.3
#define FREQUENCY 220.0

class WaveForms {
public:
  static float square(float phase) {
    if (phase > PI) {
      return 1.0;
    } else {
      return 0.0;
    }
  }

  static float saw(float phase) { return phase / TWO_PI - .5; }
};

class ADSR {
  enum class Phase { ATTACK, DECAY, RELEASE };

  Phase current_phase = Phase::ATTACK;

  float attack_time = 0.0f;
  float decay_level = 1.0f;
  float decay_time = 1.0f;
  float release_time = 1.0;

  float attack_rate = 0.0f;
  float decay_rate = 0.0f;
  float release_rate = 0.0f;

  float current_state = 0.0f;

  int sample_rate = 420000;

public:
  void set_sample_rate(int sample_rate) { this->sample_rate = sample_rate; }

  void reset() {
    this->current_state = 0.0f;
    this->current_phase = Phase::ATTACK;
  }

  void set_release(float release) {
    this->release_time = release;
    this->release_rate = 1.0f / (this->release_time * this->sample_rate);
  }

  void set_attack(float attack) {
    this->attack_time = attack;
    this->attack_rate = 1.0f / (this->attack_time * this->sample_rate);
  }

  void set_decay_level(float decay_level) { this->decay_level = decay_level; }

  void set_decay_time(float decay_time) {
    this->decay_time = decay_time;
    this->decay_rate = 1.0f / (this->decay_time * this->sample_rate);
  }

  float get_value_and_tick(bool note_on) {
    if (note_on) {

      if (this->current_phase == Phase::RELEASE) {
        this->current_phase = Phase::ATTACK;
      }

      if (this->current_phase == Phase::ATTACK) {
        this->current_state += this->attack_rate;
      } else {
        this->current_state -= this->decay_rate;
        if (this->current_state < this->decay_level) {
          this->current_state = this->decay_level;
        }
      }

      if (this->current_state >= 1.0f) {
        this->current_phase = Phase::DECAY;
      }

    } else {
      this->current_phase = Phase::RELEASE;
      this->current_state -= this->release_rate;
    }

    // Normalize
    if (this->current_state < 0.0f) {
      this->current_state = 0.0f;
    } else if (this->current_state > 1.0f) {
      this->current_state = 1.0f;
    }

    return this->current_state;
  }
};

class Operator {
public:
  Operator() {
    //    this->phase = static_cast<float>(rand()) /
    //                  (static_cast<float>(RAND_MAX / TWO_PI / 1));
  }

  void note_on(float frequency, int32_t velocity) {
    this->velocity = velocity / 127.0;
    this->setFrequency(frequency);
    this->adsr.reset();
    this->is_wave_on = true;
  }

  void setStrength(float strength) { this->strength = strength; }

  void note_off() {
    std::cout << "Operator down" << std::endl;
    this->is_wave_on = false;
  }

  void set_sample_rate(int32_t sampleRate) {
    this->sampleRate = sampleRate;
    this->adsr.set_sample_rate(sampleRate);
    this->adsr.set_attack(.01);
    this->adsr.set_decay_level(.9);
    this->adsr.set_decay_time(10);
    this->adsr.set_release(1.5);
  }

  inline float tick(float modulation) {
    float value = sin(phase + modulation) *
                  this->adsr.get_value_and_tick(is_wave_on) * this->strength *
                  this->velocity;

    phase += phase_increment * frequency_mult;
    if (phase > TWO_PI) {
      phase -= TWO_PI;
    }

    return value;
  }

  void setFrequencyMultiplier(float frequency_multiplier) {
    this->frequency_mult = frequency_multiplier;
  }

  void setFrequency(float frequency) {
    phase_increment = (TWO_PI * frequency) / (double)sampleRate;
  }

  ADSR adsr;

private:
  int32_t sampleRate;

  float velocity = 1;
  bool is_wave_on = false;
  float strength = 0.1;
  double frequency_mult = 1.0;
  double phase = 0.0;
  double phase_increment = 0.0;
};

#endif // PART1_OSCILLATOR_H
